<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "pxa_citrix_integration"
 *
 * Auto generated by Extension Builder 2017-02-20
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'Citrix Integration for TYPO3',
    'description' => '',
    'category' => 'plugin',
    'author' => 'Phat Hoang',
    'author_email' => 'phat@pixelant.se',
    'state' => 'alpha',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '8.0.0-8.9.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
