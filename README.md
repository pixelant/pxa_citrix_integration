# Pixelant

## Integration to Citrix (pxa_citrix_integration)
Adds integration to Citrix GoToWebinar.

## Configuration

### URL to the API
The baseUrl to the API is set in the Extension Manager configuration:

    apiBaseUrl = https://api.citrixonline.com

### API Credentials
The following API credentials are set in the constant editor:

    accessToken =
    organizerKey =

### Single and list view
Single view and list view pids are set in the constant editor:

    detailPid =
    listPid =

### Date format
Date formatting is done using localization with the key `date_format`.

### Minimum attedence time
The minimum attedence time in seconds sets how long an attendee needs to attend a webinar in order for the webinar to be considered *attended*. The default is 300 seconds (5minutes) and can be changed with the following typoscript constant:

    minimumAttendenceTimeInSeconds = 300

### Filtering webinars
When one shared GoTo Account is used for multiple organizers, it is possible to filter the listing of webinars based on some defined pharses. These phrases are comma separated in the typoscript constant:

    listWebinarsContaining =

## GoToWebinar API
The API for GoToWebinar can be found on https://developer.citrixonline.com/content/gotowebinar-api-reference.

### Examples of API calls
The following command fetches all webinars:

    curl -X GET --header "Accept: application/json" --header "Authorization: OAuth oauth_token={accessToken}" "https://api.citrixonline.com/G2W/rest/organizers/{organizerKey}/webinars"

### Webinar registration
The fields are fetched from the API in an array with the keys `fields` and `questions`.

*Fields:*
When outputting the label for a field, the extension uses `<f:translate key="registration_field.{field}" />`. All fields can be found on the API page.

*Questions*
Additional questions in the registration are added here in an array. The extension outputs the question by checking the questionType in the array.
The following question types are configured:

- shortAnswer
- multipleChoice

## Tests
To be able to run tests run the following first:

    composer install
    npm install

PHP codesniffer:

    npm run php:codesniffer

PHP unit test:

    npm run php:unittest

PHP functional test:

    npm run php:functionaltest

Run all tests except functional tests:

    npm run build:suite