<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function ($extKey) {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Pixelant.PxaCitrixIntegration',
            'Webinar',
            [
                'Webinar' => 'list, listWebinarsToAttend, listAttendedWebinars, detail, register, cancelRegistration'
            ],
            [
                'Webinar' => 'list, listWebinarsToAttend, listAttendedWebinars, detail, register, cancelRegistration'
            ]
        );
    },
    $_EXTKEY
);
