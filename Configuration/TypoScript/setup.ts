plugin.tx_pxacitrixintegration_webinar {
  view {
    templateRootPaths.0 = EXT:pxa_citrix_integration/Resources/Private/Templates/
    templateRootPaths.1 = {$plugin.tx_pxacitrixintegration_webinar.view.templateRootPath}
    partialRootPaths.0 = EXT:pxa_citrix_integration/Resources/Private/Partials/
    partialRootPaths.1 = {$plugin.tx_pxacitrixintegration_webinar.view.partialRootPath}
    layoutRootPaths.0 = EXT:pxa_citrix_integration/Resources/Private/Layouts/
    layoutRootPaths.1 = {$plugin.tx_pxacitrixintegration_webinar.view.layoutRootPath}
  }
  persistence {
    storagePid = {$plugin.tx_pxacitrixintegration_webinar.persistence.storagePid}
    #recursive = 1
  }
  features {
    #skipDefaultArguments = 1
  }
  mvc {
    #callDefaultActionIfActionCantBeResolved = 1
  }

  settings {
    accessToken = {$plugin.tx_pxacitrixintegration_webinar.settings.accessToken}
    organizerKey = {$plugin.tx_pxacitrixintegration_webinar.settings.organizerKey}
    lockRegistrationFields = email

    listPid = {$plugin.tx_pxacitrixintegration_webinar.settings.listPid}
    detailPid = {$plugin.tx_pxacitrixintegration_webinar.settings.detailPid}
    minimumAttendenceTimeInSeconds = {$plugin.tx_pxacitrixintegration_webinar.settings.minimumAttendenceTimeInSeconds}
    listWebinarsContaining = {$plugin.tx_pxacitrixintegration_webinar.settings.listWebinarsContaining}
  }
}
