plugin.tx_pxacitrixintegration_webinar {
  view {
    # cat=plugin.tx_pxacitrixintegration_webinar/file; type=string; label=Path to template root (FE)
    templateRootPath =
    # cat=plugin.tx_pxacitrixintegration_webinar/file; type=string; label=Path to template partials (FE)
    partialRootPath =
    # cat=plugin.tx_pxacitrixintegration_webinar/file; type=string; label=Path to template layouts (FE)
    layoutRootPath =
  }
  persistence {
    # cat=plugin.tx_pxacitrixintegration_webinar//a; type=string; label=Default storage PID
    storagePid =
  }

  settings {
    # cat=plugin.tx_pxacitrixintegration_webinar//eee; type=string; label=Access token
    accessToken =
    # cat=plugin.tx_pxacitrixintegration_webinar//eeh; type=string; label=Organizer key
    organizerKey =
    # cat=plugin.tx_pxacitrixintegration_webinar//eha; type=string; label=List pid
    listPid =
    # cat=plugin.tx_pxacitrixintegration_webinar//ehe; type=string; label=Detail pid
    detailPid =
    # cat=plugin.tx_pxacitrixintegration_webinar//ehg; type=string; label=Minimum attendence time
    minimumAttendenceTimeInSeconds = 300
    # cat=plugin.tx_pxacitrixintegration_webinar//ehj; type=string; label=Only list webinars containing these phrases (comma separated)
    listWebinarsContaining =
  }
}
