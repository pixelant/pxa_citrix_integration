<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function ($extKey) {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Pixelant.PxaCitrixIntegration',
            'Webinar',
            'Webinar'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
            $extKey,
            'Configuration/TypoScript',
            'Citrix Integration for TYPO3'
        );

        $lllFolder = 'EXT:' . $extKey . '/Resources/Private/Language';
        $fileRef = $lllFolder . '/locallang_csh_tx_pxacitrixintegration_domain_model_webinarregistration.xlf';
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
            'tx_pxacitrixintegration_domain_model_webinarregistration',
            $fileRef
        );
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages(
            'tx_pxacitrixintegration_domain_model_webinarregistration'
        );

        // Flexform for webinar plugin
        $pluginSignature = str_replace('_', '', $extKey) . '_webinar';
        $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'recursive,select_key,pages';
        $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
            $pluginSignature,
            'FILE:EXT:' . $extKey . '/Configuration/FlexForms/flexform_webinar.xml'
        );
    },
    'pxa_citrix_integration'
);
