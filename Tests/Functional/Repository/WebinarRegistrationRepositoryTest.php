<?php
namespace Pixelant\PxaCitrixIntegration\Tests\Functional\Repository;

/***
 *
 * This file is part of the "Citrix Integration for TYPO3" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Phat Hoang <phat@pixelant.se>, Pixelant
 *
 ***/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use Pixelant\PxaCitrixIntegration\Domain\Repository\WebinarRegistrationRepository;
use Pixelant\PxaCitrixIntegration\Domain\Model\WebinarRegistration;
use Pixelant\PxaCitrixIntegration\Citrix\Request\WebinarRequest;

/**
 * Functional test for WebinarRegistrationRepository
 */
class WebinarRegistrationRepositoryTest extends \TYPO3\CMS\Core\Tests\FunctionalTestCase
{

    /** @var WebinarRegistrationRepository */
    protected $webinarRegistrationRepository;

    /** @var PersistenceManager */
    protected $persistenceManager;

    /** @var array  */
    protected $testExtensionsToLoad = ['typo3conf/ext/pxa_citrix_integration'];

    /**
     * Setup
     *
     * @throws \TYPO3\CMS\Core\Tests\Exception
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        /** @var ObjectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);

        $this->persistenceManager = $objectManager->get(PersistenceManager::class);

        // Create the subject and disable respectStoragePage
        $querySettings = $objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings::class);
        $querySettings->setRespectStoragePage(false);

        $this->webinarRegistrationRepository = $objectManager->get(WebinarRegistrationRepository::class);
        $this->webinarRegistrationRepository->setDefaultQuerySettings($querySettings);

        $this->importDataSet(__DIR__ . '/../Fixtures/tx_pxacitrixintegration_domain_model_webinarregistration.xml');
    }

    /**
     * @test
     */
    public function createWebinarRegistrationReturnsWebinarRegistrationObjectOnSuccess()
    {
        $webinarKey = '999';
        $email = 'email@domain.tld';
        $registrantKey = 'A registrant key';

        $webinarRegistration = $this->webinarRegistrationRepository->createWebinarRegistration(
            $webinarKey,
            $registrantKey,
            $email
        );

        $this->assertTrue($webinarRegistration instanceof WebinarRegistration);
    }

    /**
     * @test
     */
    public function createWebinarRegistrationIsSavedAfterPersist()
    {
        $countBefore = $this->webinarRegistrationRepository->findAll()->count();
        $expectedCount = $countBefore + 1;

        // If it's not greater than 0 probably the findAll is broken
        $this->assertGreaterThan(0, $countBefore);

        $webinarKey = '999';
        $email = 'email@domain.tld';
        $registrantKey = 'A registrant key';

        $this->webinarRegistrationRepository->createWebinarRegistration(
            $webinarKey,
            $registrantKey,
            $email
        );

        // Persist and get count again
        $this->persistenceManager->persistAll();
        $newCount = $this->webinarRegistrationRepository->findAll()->count();

        $this->assertEquals($newCount, $expectedCount);
    }

    /**
     * @test
     */
    public function findByUidReturnsExpectedWebinarRegistration()
    {
        $webinarRegistration = $this->webinarRegistrationRepository->findByUid(1);
        $this->assertEquals($webinarRegistration->getWebinarKey(), 1);
        $this->assertEquals($webinarRegistration->getEmail(), 'email-one@domain.tld');
    }

    /**
     * @test
     */
    public function removeWebinarRegistration()
    {
        $webinarKey = 1;
        $registrantKey = 'Registrant Key One';
        $email = 'email-one@domain.tld';

        $this->webinarRegistrationRepository->removeWebinarRegistration($webinarKey, $registrantKey, $email);
        $this->persistenceManager->persistAll();

        $this->assertNull($this->webinarRegistrationRepository->findByUid(1));
    }

    /**
     * @test
     */
    public function removeWebinarRegistrationReturnsCorrectBool()
    {
        // Test successful removal
        $webinarKey = 1;
        $registrantKey = 'Registrant Key One';
        $email = 'email-one@domain.tld';
        $this->assertTrue(
            $this->webinarRegistrationRepository->removeWebinarRegistration($webinarKey, $registrantKey, $email)
        );

        // Test failure removal with nonexisting email
        $webinarKey = 1;
        $registrantKey = 'Registrant Key One';
        $email = 'nonExistingEmail@domain.tld';
        $this->assertFalse(
            $this->webinarRegistrationRepository->removeWebinarRegistration($webinarKey, $registrantKey, $email)
        );
    }
}
