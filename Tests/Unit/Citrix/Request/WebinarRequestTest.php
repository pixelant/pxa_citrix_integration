<?php
namespace Pixelant\PxaCitrixIntegration\Tests\Unit\Citrix\Request;

/***
 *
 * This file is part of the "Citrix Integration for TYPO3" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Phat Hoang <phat@pixelant.se>, Pixelant
 *
 ***/

use Pixelant\PxaCitrixIntegration\Citrix\Request\WebinarRequest;

/**
 * WebinarRequestTest
 */
class WebinarRequestTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{

    /**
     * @test
     */
    public function getRegistrantByEmailUserNotRegistered()
    {
        $webinarKey = 'A webinar key';

        $registrants = [
            [
                'name' => 'A first name',
                'email' => 'an-email@domain.tld'
            ],
            [
                'name' => 'A second name',
                'email' => 'another-email@domain.tld'
            ],
        ];

        $input = 'non-existing-email@domain.tld';
        $expected = [];

        $subject = $this->getMockBuilder(WebinarRequest::class)
            ->setMethods(['getRegistrants'])
            ->disableOriginalConstructor()
            ->getMock();

        $subject->expects($this->once())->method('getRegistrants')->will($this->returnValue($registrants));
        $this->assertEquals($expected, $subject->getRegistrantByEmail($webinarKey, $input));
    }

    /**
     * @test
     */
    public function getRegistrantByEmailRegistrantFound()
    {
        $webinarKey = 'A webinar key';

        $registrants = [
            [
                'name' => 'A first name',
                'email' => 'an-email@domain.tld'
            ],
            [
                'name' => 'Name to be found',
                'email' => 'e-mail-to-be-found@domain.tld'
            ],
        ];

        $input = 'e-mail-to-be-found@domain.tld';
        $expected = [
            'name' => 'Name to be found',
            'email' => 'e-mail-to-be-found@domain.tld'
        ];

        $subject = $this->getMockBuilder(WebinarRequest::class)
            ->setMethods(['getRegistrants'])
            ->disableOriginalConstructor()
            ->getMock();

        $subject->expects($this->once())->method('getRegistrants')->will($this->returnValue($registrants));
        $this->assertEquals($expected, $subject->getRegistrantByEmail($webinarKey, $input));
    }
}
