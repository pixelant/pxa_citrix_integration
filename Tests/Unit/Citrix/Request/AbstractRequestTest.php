<?php
namespace Pixelant\PxaCitrixIntegration\Tests\Unit\Citrix\Request;

/***
 *
 * This file is part of the "Citrix Integration for TYPO3" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Phat Hoang <phat@pixelant.se>, Pixelant
 *
 ***/

use Pixelant\PxaCitrixIntegration\Citrix\Request\AbstractRequest;
use Pixelant\PxaCitrixIntegration\Service\ConfigurationService;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * AbstractRequestTest
 */
class AbstractRequestTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var array A backup of registered singleton instances because setSingletonInstance is used
     */
    protected $singletonInstances = [];

    /**
     * Setup
     *
     * @return void
     */
    protected function setUp()
    {
        $this->singletonInstances = \TYPO3\CMS\Core\Utility\GeneralUtility::getSingletonInstances();
    }

    /**
     * Teardown
     *
     * @return void
     */
    protected function tearDown()
    {
        GeneralUtility::resetSingletonInstances($this->singletonInstances);
        parent::tearDown();
    }

    /**
     * @expectedException \Exception
     * @test
     */
    public function missingApiBaseUrl()
    {
        // Dummy credentials
        $accessToken = 'a token';
        $organizerKey = 'a key';
        $this->getAccessibleMockForAbstractClass(
            AbstractRequest::class,
            [$accessToken, $organizerKey]
        );
    }

    /**
     * @test
     */
    public function credentialAddedInRequestHeader()
    {
        // Dummy credential ($accessToken needs to be added in the http header)
        $accessToken = 'a token';
        $organizerKey = 'a key';

        // Inject extensionConfiguration with dummy apiBaseUrl
        $configurationServiceMock = $this->getAccessibleMock(ConfigurationService::class, [], [], '', false);
        $configurationServiceMock->expects($this->once())->method('getExtensionConfiguration')->will($this->returnValue(
            [
                'apiBaseUrl' => 'https://domain.com'
            ]
        ));
        GeneralUtility::setSingletonInstance(ConfigurationService::class, $configurationServiceMock);

        // Mock the abstract class and get the headers
        $subject = $this->getAccessibleMockForAbstractClass(
            AbstractRequest::class,
            [$accessToken, $organizerKey]
        );
        $requestHeaders = $subject->_call('getRequestHeaders');

        $this->assertEquals(
            'OAuth oauth_token=' . $accessToken,
            $requestHeaders['Authorization']
        );
    }

    /**
     * @test
     */
    public function overrideRequestHeadersCanAddHeader()
    {
        // Dummy credential ($accessToken needs to be added in the http header)
        $accessToken = 'a token';
        $organizerKey = 'a key';

        // Inject extensionConfiguration with dummy apiBaseUrl
        $configurationServiceMock = $this->getAccessibleMock(ConfigurationService::class, [], [], '', false);
        $configurationServiceMock->expects($this->once())->method('getExtensionConfiguration')->will($this->returnValue(
            [
                'apiBaseUrl' => 'https://domain.com'
            ]
        ));
        GeneralUtility::setSingletonInstance(ConfigurationService::class, $configurationServiceMock);

        // Mock the abstract class and get the headers
        $subject = $this->getAccessibleMockForAbstractClass(
            AbstractRequest::class,
            [$accessToken, $organizerKey]
        );

        $input = [
            'newKey' => 'newValue'
        ];

        $requestHeaders = $subject->_call('getRequestHeaders', $input);

        // Check if key exists in the returned requestHeaders and if value is the same
        $this->assertTrue(array_key_exists('newKey', $requestHeaders));
        $this->assertEquals($input['newKey'], $requestHeaders['newKey']);
    }

    /**
     * @test
     */
    public function overrideRequestHeadersCanOverrideHeader()
    {
        // Dummy credential ($accessToken needs to be added in the http header)
        $accessToken = 'a token';
        $organizerKey = 'a key';

        // Inject extensionConfiguration with dummy apiBaseUrl
        $configurationServiceMock = $this->getAccessibleMock(ConfigurationService::class, [], [], '', false);
        $configurationServiceMock->expects($this->once())->method('getExtensionConfiguration')->will($this->returnValue(
            [
                'apiBaseUrl' => 'https://domain.com'
            ]
        ));
        GeneralUtility::setSingletonInstance(ConfigurationService::class, $configurationServiceMock);

        // Mock the abstract class and get the headers
        $subject = $this->getAccessibleMockForAbstractClass(
            AbstractRequest::class,
            [$accessToken, $organizerKey]
        );

        $input = [
            'Content-type' => 'a new value',
        ];

        $requestHeaders = $subject->_call('getRequestHeaders', $input);

        // Check if value is overriden
        $this->assertEquals($input['Content-type'], $requestHeaders['Content-type']);
    }
}
