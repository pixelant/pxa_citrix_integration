<?php
namespace Pixelant\PxaCitrixIntegration\Tests\Unit\ViewHelpers\Format;

/***
 *
 * This file is part of the "Citrix Integration for TYPO3" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Phat Hoang <phat@pixelant.se>, Pixelant
 *
 ***/

use Pixelant\PxaCitrixIntegration\ViewHelpers\Format\DateViewHelper;
use TYPO3\CMS\Fluid\Tests\Unit\ViewHelpers\ViewHelperBaseTestcase;

/**
 * DateViewHelperTest
 */
class DateViewHelperTest extends ViewHelperBaseTestcase
{
    /**
     * @var DateViewHelper
     */
    protected $viewHelper;

    /**
     * Setup
     *
     * @return void
     */
    protected function setUp()
    {
        parent::setUp();
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['ddmmyy'] = 'Y-m-d';
        $this->viewHelper = new DateViewHelper();
        $this->injectDependenciesIntoViewHelper($this->viewHelper);
    }

   /**
     * Data provider for formatDateRespectTimeZoneAndSummerWinterTime
     *
     * @return array
     */
    public function formatDateRespectTimeZoneAndSummerWinterTimeDataProvider()
    {
        return [
            'Europe/Stockholm Summer Time' => [
                [
                    'timeZone' => 'Europe/Stockholm',
                    'format' => 'd/m/Y H:i T',
                    'date' => '2017-05-01T12:30:00Z'
                ],
                '01/05/2017 14:30 CEST'
            ],
            'Europe/London Summer Time' => [
                [
                    'timeZone' => 'Europe/London',
                    'format' => 'd/m/Y H:i T',
                    'date' => '2017-05-01T12:30:00Z',
                ],
                '01/05/2017 13:30 BST'
            ],
            'Europe/Stockholm Winter Time' => [
                [
                    'timeZone' => 'Europe/Stockholm',
                    'format' => 'd/m/Y H:i T',
                    'date' => '2017-02-01T12:30:00Z'
                ],
                '01/02/2017 13:30 CET'
            ],
            'Europe/London Winter Time' => [
                [
                    'timeZone' => 'Europe/London',
                    'format' => 'd/m/Y H:i T',
                    'date' => '2017-02-01T12:30:00Z',
                ],
                '01/02/2017 12:30 GMT'
            ],
        ];
    }

    /**
     * @test
     * @dataProvider formatDateRespectTimeZoneAndSummerWinterTimeDataProvider
     */
    public function formatDateRespectTimeZoneAndSummerWinterTime($input, $expected)
    {
        $this->setArgumentsUnderTest($this->viewHelper, $input);
        $actualResult = $this->viewHelper->initializeArgumentsAndRender();
        $this->assertEquals($expected, $actualResult);
    }
}
