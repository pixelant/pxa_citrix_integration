<?php
namespace Pixelant\PxaCitrixIntegration\Tests\Unit\Controller\WebinarController;

/***
 *
 * This file is part of the "Citrix Integration for TYPO3" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Phat Hoang <phat@pixelant.se>, Pixelant
 *
 ***/

use Pixelant\PxaCitrixIntegration\Controller\WebinarController;
use Pixelant\PxaCitrixIntegration\Citrix\Request\WebinarRequest;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;

/**
 * WebinarControllerTest
 */
class WebinarControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{

    /**
     * Data provider for lockAField
     */
    public function lockAFieldDataProvider()
    {
        return [
            'Just the field' => [
                ['lockRegistrationFields' => 'fieldToBeLocked']
            ],
            'Field in the beginning' => [
                ['lockRegistrationFields' => 'fieldToBeLocked,anotherField']
            ],
            'Field in the middle' => [
                ['lockRegistrationFields' => 'anotherField,fieldToBeLocked,lastField']
            ],
            'Field in the end' => [
                ['lockRegistrationFields' => 'anotherField,middleField,fieldToBeLocked']
            ],
            'Space in comma separated string' => [
                ['lockRegistrationFields' => 'fieldToBeLocked, anotherField']
            ],
        ];
    }

    /**
     * @test
     * @dataProvider lockAFieldDataProvider
     */
    public function lockAField($typoscriptSetting)
    {
        $input = [
            'fields' => [
                [
                    'field' => 'aField',
                    'maxSize' => 128,
                    'required' => true
                ],
                [
                    'field' => 'fieldToBeLocked',
                    'maxSize' => 128,
                    'required' => true
                ]
            ]
        ];

        $expected = [
            'fields' => [
                [
                    'field' => 'aField',
                    'maxSize' => 128,
                    'required' => true
                ],
                [
                    'field' => 'fieldToBeLocked',
                    'maxSize' => 128,
                    'required' => true,
                    'locked' => true
                ]
            ]
        ];

        $subject = $this->getAccessibleMock(WebinarController::class, ['dummy'], [], '', false);
        $subject->_set('settings', $typoscriptSetting);
        $this->assertEquals($expected, $subject->_call('lockRegistrationFields', $input));
    }


    /**
     * @test
     */
    public function lockTwoFields()
    {
        $typoscriptSetting = [
            'lockRegistrationFields' => 'fieldOneToBeLocked, fieldTwoToBeLocked'
        ];

        $input = [
            'fields' => [
                [
                    'field' => 'fieldOneToBeLocked',
                    'maxSize' => 128,
                    'required' => true
                ],
                [
                    'field' => 'aField',
                    'maxSize' => 128,
                    'required' => true
                ],
                [
                    'field' => 'fieldTwoToBeLocked',
                    'maxSize' => 128,
                    'required' => true,
                ]
            ]
        ];

        $expected = [
            'fields' => [
                [
                    'field' => 'fieldOneToBeLocked',
                    'maxSize' => 128,
                    'required' => true,
                    'locked' => true
                ],
                [
                    'field' => 'aField',
                    'maxSize' => 128,
                    'required' => true
                ],
                [
                    'field' => 'fieldTwoToBeLocked',
                    'maxSize' => 128,
                    'required' => true,
                    'locked' => true
                ]
            ]
        ];

        $subject = $this->getAccessibleMock(WebinarController::class, ['dummy'], [], '', false);
        $subject->_set('settings', $typoscriptSetting);
        $this->assertEquals($expected, $subject->_call('lockRegistrationFields', $input));
    }

    /**
     * @test
     */
    public function listActionFiltersWebinar()
    {
        $subject = $this->getAccessibleMock(WebinarController::class, ['dummy'], [], '', false);

        $settings = [
            'listWebinarsContaining' => 'Is Visible, Is VisibleToo'
        ];
        $this->inject($subject, 'settings', $settings);

        $input = [
            [
                'subject' => 'Webinar 1 Is Visible'
            ],
            [
                'subject' => 'Webinar 2 Is Not Visible'
            ],
            [
                'subject' => 'Webinar 3 Is VisibleToo'
            ]
        ];

        $expected = [
            [
                'subject' => 'Webinar 1 Is Visible'
            ],
            [
                'subject' => 'Webinar 3 Is VisibleToo'
            ]
        ];

        $webinarRequest = $this->getAccessibleMock(WebinarRequest::class, ['getAllWebinars'], [], '', false);
        $webinarRequest->expects($this->once())->method('getAllWebinars')->will($this->returnValue($input));
        $this->inject($subject, 'webinarRequest', $webinarRequest);

        $view = $this->createMock(ViewInterface::class);
        $view->expects($this->at(0))->method('assign')->with('webinars', $expected);
        $this->inject($subject, 'view', $view);

        $subject->listAction();
    }

    /**
     * @test
     */
    public function listActionFiltersWebinarNotCaseSensitive()
    {
        $subject = $this->getAccessibleMock(WebinarController::class, ['dummy'], [], '', false);

        $settings = [
            'listWebinarsContaining' => 'IS VISIBLE, IS VISIBLETOO'
        ];
        $this->inject($subject, 'settings', $settings);

        $input = [
            [
                'subject' => 'Webinar 1 Is Visible'
            ],
            [
                'subject' => 'Webinar 2 Is Not Visible'
            ],
            [
                'subject' => 'Webinar 3 Is VisibleToo'
            ]
        ];

        $expected = [
            [
                'subject' => 'Webinar 1 Is Visible'
            ],
            [
                'subject' => 'Webinar 3 Is VisibleToo'
            ]
        ];

        $webinarRequest = $this->getAccessibleMock(WebinarRequest::class, ['getAllWebinars'], [], '', false);
        $webinarRequest->expects($this->once())->method('getAllWebinars')->will($this->returnValue($input));
        $this->inject($subject, 'webinarRequest', $webinarRequest);

        $view = $this->createMock(ViewInterface::class);
        $view->expects($this->at(0))->method('assign')->with('webinars', $expected);
        $this->inject($subject, 'view', $view);

        $subject->listAction();
    }

    /**
     * @test
     */
    public function listActionFiltersWebinarEmptySettings()
    {
        $subject = $this->getAccessibleMock(WebinarController::class, ['dummy'], [], '', false);

        $settings = [];
        $this->inject($subject, 'settings', $settings);

        $input = [
            [
                'subject' => 'Webinar 1'
            ],
            [
                'subject' => 'Webinar 2'
            ],
            [
                'subject' => 'Webinar 3'
            ]
        ];

        $expected = [
            [
                'subject' => 'Webinar 1'
            ],
            [
                'subject' => 'Webinar 2'
            ],
            [
                'subject' => 'Webinar 3'
            ]
        ];

        $webinarRequest = $this->getAccessibleMock(WebinarRequest::class, ['getAllWebinars'], [], '', false);
        $webinarRequest->expects($this->once())->method('getAllWebinars')->will($this->returnValue($input));
        $this->inject($subject, 'webinarRequest', $webinarRequest);

        $view = $this->createMock(ViewInterface::class);
        $view->expects($this->at(0))->method('assign')->with('webinars', $expected);
        $this->inject($subject, 'view', $view);

        $subject->listAction();
    }
}
