<?php
namespace Pixelant\PxaCitrixIntegration\Citrix\Request;

/***
 *
 * This file is part of the "Citrix Integration for TYPO3" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Phat Hoang <phat@pixelant.se>, Pixelant
 *
 ***/

/**
 * Webinar
 */
class WebinarRequest extends AbstractRequest
{

    const STATUS_CODE_CANCEL_REGISTRATION_NOT_REGISTERED = 200;
    const STATUS_CODE_CANCEL_REGISTRATION_SUCCESS = 204;

    /**
     * @var string
     */
    protected $apiUrl = 'G2W/rest';

    /**
     * Returns webinars scheduled for the future for a specified organizer
     *
     * @return array
     */
    public function getAllWebinars()
    {
        $webinars = [];
        $request = $this->get([
            'organizers',
            $this->organizerKey,
            'webinars'
        ]);

        if ($request['success']) {
            $webinars = $request['body'];
        }

        return $webinars;
    }

    /**
     * Retrieve information on a specific webinar
     *
     * @param string $webinarKey
     * @return array
     */
    public function getWebinar($webinarKey)
    {
        $webinar = [];

        $request = $this->get([
            'organizers',
            $this->organizerKey,
            'webinars',
            $webinarKey
        ]);

        if ($request['success']) {
            $webinar = $request['body'];
        }

        return $webinar;
    }

    /**
     * Retrieve registration details for all registrants of a specific webinar
     * Registrant details will not include all fields captured when creating the registrant
     *
     * @param string $webinarKey
     * @return array
     */
    public function getRegistrants($webinarKey)
    {
        $registrants = [];

        $request = $this->get([
            'organizers',
            $this->organizerKey,
            'webinars',
            $webinarKey,
            'registrants'
        ]);

        if ($request['success']) {
            $registrants = $request['body'];
        }

        return $registrants;
    }

    /**
     * Retrieve required, optional registration, and custom questions for a specified webinar
     *
     * @param string $webinarKey
     * @return array
     */
    public function getRegistrationFields($webinarKey)
    {
        $registrationFields = [];

        $request = $this->get([
            'organizers',
            $this->organizerKey,
            'webinars',
            $webinarKey,
            'registrants',
            'fields'
        ]);

        if ($request['success']) {
            $registrationFields = $request['body'];
        }

        return $registrationFields;
    }

    /**
     * Register an attendee for a scheduled webinar.
     *
     * @param string $webinarKey
     * @param array $registrationFields
     * @return array
     */
    public function register($webinarKey, $registrationFields)
    {
        if (empty($webinarKey)) {
            throw new \Exception('Missing required parameter: \'webinarKey\'', 5409353100);
        }

        if (empty($registrationFields)) {
            throw new \Exception('Missing required parameter: \'registrationFields\'', 5409353110);
        }

        // If there is survey then this flag will send another Accept-header required by the API
        $allowCustomFields = !empty($registrationFields['responses']);

        $request = $this->post(
            [
                'organizers',
                $this->organizerKey,
                'webinars',
                $webinarKey,
                'registrants'
            ],
            $registrationFields,
            $allowCustomFields
        );

        return $request;
    }

    /**
     * Get registrant from a webinar
     *
     * @param string $webinarKey
     * @param string $registrantKey
     * @return array
     */
    public function getRegistrant($webinarKey, $registrantKey)
    {
        if (empty($webinarKey)) {
            throw new \Exception('Missing required parameter: \'webinarKey\'', 5409353280);
        }

        if (empty($registrantKey)) {
            throw new \Exception('Missing required parameter: \'registrantKey\'', 5409353290);
        }

        $registrant = [];

        $request = $this->get(
            [
                'organizers',
                $this->organizerKey,
                'webinars',
                $webinarKey,
                'registrants',
                $registrantKey
            ]
        );

        if ($request['success']) {
            $registrant = $request['body'];
        }

        return $registrant;
    }

    /**
     * Remove registrant from a webinar
     *
     * @param string $webinarKey
     * @param string $registrantKey
     * @return array
     */
    public function removeRegistrant($webinarKey, $registrantKey)
    {
        if (empty($webinarKey)) {
            throw new \Exception('Missing required parameter: \'webinarKey\'', 5409353200);
        }

        if (empty($registrantKey)) {
            throw new \Exception('Missing required parameter: \'registrantKey\'', 5409353210);
        }

        $request = $this->delete(
            [
                'organizers',
                $this->organizerKey,
                'webinars',
                $webinarKey,
                'registrants',
                $registrantKey
            ]
        );

        // Modify the success because both 200 and 204 are success but only 204 is a successful request in this case
        $request['success'] = $request['success'] &&
            $request['statusCode'] === self::STATUS_CODE_CANCEL_REGISTRATION_SUCCESS;

        return $request;
    }

    /**
     * Get registration info by email
     *
     * @param string $webinarKey
     * @param string $registrationFields
     * @return array
     */
    public function getRegistrantByEmail($webinarKey, $email)
    {
        if (empty($webinarKey)) {
            throw new \Exception('Missing required parameter: \'webinarKey\'', 5409353100);
        }

        if (empty($email)) {
            throw new \Exception('Missing required parameter: \'email\'', 5409353110);
        }

        // Get all registrants and loop through them to check if email is registered
        $registrants = $this->getRegistrants($webinarKey);
        foreach ($registrants as $registrant) {
            if (strcasecmp($email, $registrant['email']) === 0) {
                return $registrant;
            }
        }
        return [];
    }

    /**
     * Get attendee sessions for a given registrant in a given webinar
     * This is a heave call
     *
     * @param string $webinarKey
     * @param int $registrantKey
     * @return array
     */
    public function getAttendeeSessions($webinarKey, $registrantKey)
    {
        if (empty($webinarKey)) {
            throw new \Exception('Missing required parameter: \'webinarKey\'', 5409353120);
        }

        if (empty($registrantKey)) {
            throw new \Exception('Missing required parameter: \'registrantKey\'', 5409353130);
        }

        $attendeeSessions = [];

        // Get all sessions of the webinar
        $request = $this->get(
            [
                'organizers',
                $this->organizerKey,
                'webinars',
                $webinarKey,
                'sessions'
            ]
        );

        if ($request['success'] && is_array($request['body'])) {
            $webinarSessions = $request['body'];

            // Go through all sessions and for each session get the attendees
            foreach ($webinarSessions as $webinarSession) {
                $sessionKey = $webinarSession['sessionKey'];
                $request = $this->get(
                    [
                        'organizers',
                        $this->organizerKey,
                        'webinars',
                        $webinarKey,
                        'sessions',
                        $sessionKey,
                        'attendees',
                    ]
                );

                if ($request['success'] && is_array($request['body'])) {
                    $attendees = $request['body'];

                    // For each attendee check if the registrantKey is the same
                    foreach ($attendees as $attendee) {
                        if ($attendee['registrantKey'] === $registrantKey) {
                            $attendeeSessions[] = $attendee;
                        }
                    }
                }
            }
        }
        return $attendeeSessions;
    }
}
