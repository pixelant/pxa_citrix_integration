<?php
namespace Pixelant\PxaCitrixIntegration\Citrix\Request;

/***
 *
 * This file is part of the "Citrix Integration for TYPO3" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Phat Hoang <phat@pixelant.se>, Pixelant
 *
 ***/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\ArrayUtility;

/**
 * AbstractRequest
 */
class AbstractRequest
{
    /**
     * @var string
     */
    protected $apiBaseUrl;

    /**
     * @var string
     */
    protected $accessToken;

    /**
     * @var string
     */
    protected $organizerKey;

    /**
     * @var string
     */
    protected $apiUrl;

    /**
     * requestFactory
     *
     * @var TYPO3\CMS\Core\Http\RequestFactory
     */
    protected $requestFactory;

    /**
     * Constructor
     *
     * @throws \Exception
     */
    public function __construct($accessToken, $organizerKey)
    {
        /** @var \Pixelant\PxaCitrixIntegration\Service\ConfigurationService::class */
        $configurationService = GeneralUtility::makeInstance(
            \Pixelant\PxaCitrixIntegration\Service\ConfigurationService::class
        );
        $extensionConfiguration = $configurationService->getExtensionConfiguration();

        if (empty($extensionConfiguration['apiBaseUrl'])) {
            throw new \Exception('Missing apiBaseUrl', 5309353910);
        }

        if (empty($accessToken)) {
            throw new \Exception('Missing accessToken', 5309353920);
        }

        if (empty($organizerKey)) {
            throw new \Exception('Missing organizerKey', 5309353930);
        }

        $this->apiBaseUrl = $extensionConfiguration['apiBaseUrl'];
        $this->accessToken = $accessToken;
        $this->organizerKey = $organizerKey;

        $this->requestFactory = GeneralUtility::makeInstance(
            \TYPO3\CMS\Core\Http\RequestFactory::class
        );
    }

    /**
     * GetRequest
     *
     * @param array $route
     * @throws \Exception
     * @return array
     */
    protected function get($route)
    {
        if (empty($route)) {
            throw new \Exception('Missing required parameter: \'route\'', 5309353940);
        }

        return $this->executeRequest(
            implode($route, '/'),
            'GET'
        );
    }

    /**
     * PostRequest
     *
     * @param array $route
     * @param array $data
     * @param bool $allowCustomFields
     * @throws \Exception
     * @return array
     */
    protected function post($route, $data, $allowCustomFields = false)
    {
        if (empty($route)) {
            throw new \Exception('Missing required parameter: \'route\'', 5309353950);
        }

        if (empty($data)) {
            throw new \Exception('Missing required parameter: \'data\'', 5309353960);
        }

        return $this->executeRequest(
            implode($route, '/'),
            'POST',
            $data,
            $allowCustomFields
        );
    }

    /**
     * DeleteRequest
     *
     * @param array $route
     * @throws \Exception
     * @return array
     */
    protected function delete($route)
    {
        if (empty($route)) {
            throw new \Exception('Missing required parameter: \'route\'', 5309353990);
        }

        return $this->executeRequest(
            implode($route, '/'),
            'DELETE'
        );
    }

    /**
     * ExecuteRequest
     *
     * @param string $url
     * @param string $method
     * @param array $data
     * @param bool $allowCustomFields
     * @return array
     */
    protected function executeRequest($url, $method, $data = [], $allowCustomFields = false)
    {
        // Set the full url, headers and make the request
        $requestUrl = $this->apiBaseUrl . '/' . $this->apiUrl . '/' . $url;

        $additionalOptions = [
            'headers' => $this->getRequestHeaders(
                $allowCustomFields ? ['Accept' => 'application/vnd.citrix.g2wapi-v1.1+json'] : []
            ),
            'json' => $data
        ];

        try {
            $response = $this->requestFactory->request($requestUrl, $method, $additionalOptions);
            if ($response->getStatusCode() > 400) {
                return [
                    'success' => false,
                    'statusCode' => $response->getStatusCode()
                ];
            } else {
                return [
                    'success' => true,
                    'body' => json_decode($response->getBody()->getContents(), true),
                    'statusCode' => $response->getStatusCode()
                ];
            }
        } catch (\Exception $e) {
            return [
                'success' => false,
                'statusCode' => $e->getCode(),
                'exception' => $e
            ];
        }
    }

    /**
     * Gets HTTP headers with API credential for requests
     *
     * @param array $overrideHeaders
     * @return array
     */
    protected function getRequestHeaders($overrideHeaders = [])
    {
        $requestHeaders = [
            'Content-type' => 'application/json',
            'Authorization' => 'OAuth oauth_token=' . $this->accessToken,
            'Accept' => 'application/json'
        ];

        ArrayUtility::mergeRecursiveWithOverrule($requestHeaders, $overrideHeaders);
        return $requestHeaders;
    }
}
