<?php
namespace Pixelant\PxaCitrixIntegration\Controller;

/***
 *
 * This file is part of the "Citrix Integration for TYPO3" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Phat Hoang <phat@pixelant.se>, Pixelant
 *
 ***/

use Pixelant\PxaCitrixIntegration\Domain\Model\Webinar;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use Pixelant\PxaCitrixIntegration\Utility\CitrixUtility;
use Pixelant\PxaCitrixIntegration\Citrix\Request\WebinarRequest;
use Pixelant\PxaCitrixIntegration\Domain\Model\WebinarRegistration;

/**
 * WebinarController
 */
class WebinarController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * @var array
     */
    protected $feUserFieldToGetter = [
        'firstName' => 'getFirstName',
        'lastName' => 'getLastName',
        'email' => 'getEmail'
    ];

    /**
     * @var array
     */
    protected $lockedFieldsArray;

    /**
     * Webinar request
     *
     * @var WebinarRequest
     */
    protected $webinarRequest;

    /**
     * frontendUserRepository
     *
     * @var TYPO3\CMS\Extbase\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $frontendUserRepository = null;

    /**
     * @var TYPO3\CMS\Extbase\Domain\Model\FrontendUser
     */
    protected $frontendUser;

    /**
     * webinarRegistrationRepository
     *
     * @var Pixelant\PxaCitrixIntegration\Domain\Repository\WebinarRegistrationRepository
     * @inject
     */
    protected $webinarRegistrationRepository = null;

    /**
     * initializeAction
     *
     * @return void
     */
    public function initializeAction()
    {
        $this->webinarRequest = GeneralUtility::makeInstance(
            WebinarRequest::class,
            $this->settings['accessToken'],
            $this->settings['organizerKey']
        );

        if ($GLOBALS['TSFE']->fe_user->user) {
            $this->frontendUser = $this->frontendUserRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']);
        }
    }

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $allWebinars = $this->webinarRequest->getAllWebinars();
        $webinars = [];

        // Only show webinars where the title (subject) contains specific phrases
        // if settings.listWebinarsContaining is set
        if (!empty($this->settings['listWebinarsContaining'])) {
            $phrases = GeneralUtility::trimExplode(',', $this->settings['listWebinarsContaining']);
            foreach ($allWebinars as $key => $webinar) {
                foreach ($phrases as $phrase) {
                    if (stripos($webinar['subject'], $phrase) !== false) {
                        $webinars[] = $webinar;
                        break;
                    }
                }
            }
        } else {
            $webinars = $allWebinars;
        }

        $this->view->assign('webinars', $webinars);
    }

    /**
     * List webinars to attend
     *
     * @return void
     */
    public function listWebinarsToAttendAction()
    {
        $webinarsToAttend = $this->webinarRegistrationRepository->findWebinardsToAttend(
            $this->frontendUser->getEmail()
        );

        $this->view->assign('webinars', $webinarsToAttend);
    }

    /**
     * List attended webinars
     *
     * @return void
     */
    public function listAttendedWebinarsAction()
    {
        $webinars = $this->webinarRegistrationRepository->findAttendedWebinars(
            $this->frontendUser->getEmail(),
            $this->settings['minimumAttendenceTimeInSeconds']
        );

        $this->view->assign('webinars', $webinars);
    }

    /**
     * action detail
     *
     * @param int $webinarKey
     * @return void
     */
    public function detailAction($webinarKey)
    {
        if (empty($webinarKey)) {
            throw new \Exception('Missing required parameter \'webinarKey\'', 43873404910);
        }

        $webinar = $this->webinarRequest->getWebinar($webinarKey);
        $registrant = $this->webinarRequest->getRegistrantByEmail($webinarKey, $this->frontendUser->getEmail());

        $this->view->assignMultiple([
            'webinar' => $webinar,
        ]);

        // If there is registrantInfo then user is registered
        if (!empty($registrant)) {
            $this->view->assign('registrant', $registrant);
        } else {
            // Get registration fields with API
            $registrationFields = $this->webinarRequest->getRegistrationFields($webinarKey);

            // Prefill with feuser data
            $registrationFields = $this->prefillRegistrationFields($registrationFields);

            // Make some fields readonly
            $registrationFields = $this->lockRegistrationFields($registrationFields);

            $this->view->assignMultiple([
                'registrationFields' => $registrationFields,
                'registrants' => $registrants,
            ]);
        }
    }

    /**
     * action register
     *
     * @param int $webinarKey
     * @param array $registrationFields
     * @return void
     */
    public function registerAction($webinarKey, $registrationFields)
    {
        if (empty($webinarKey)) {
            throw new \Exception('Missing required parameter \'webinarKey\'', 43873404920);
        }

        $success = false;
        $result = [];

        if (!empty($registrationFields)) {
            // Check forced fields and force them
            foreach ($registrationFields as $field => $value) {
                if ($this->isFieldLocked($field)) {
                    $registrationFields[$field] = $this->frontendUser->{$this->feUserFieldToGetter[$field]}();
                }
            }

            // If there is survey then remove the keys from since it's required by the API
            if (!empty($registrationFields['responses'])) {
                $registrationFields['responses'] = array_values($registrationFields['responses']);
            }

            $result = $this->webinarRequest->register($webinarKey, $registrationFields);
            if (!empty($result)) {
                $success = $result['success'];
            }
        }

        if ($success) {
            // Save registration in database
            $this->webinarRegistrationRepository->createWebinarRegistration(
                $webinarKey,
                $result['body']['registrantKey'],
                $this->frontendUser->getEmail()
            );

            $this->addFlashMessage(
                CitrixUtility::translate('registration_webinar.success.message'),
                '',
                AbstractMessage::OK
            );

            $this->redirect(
                'detail',
                null,
                null,
                [
                    'webinarKey' => $webinarKey
                ]
            );
        } else {
            // If request failed then there must be statusCode so throw exception if
            // there is no statusCode
            if (empty($result['statusCode'])) {
                throw new \Exception('Missing statusCode', 3907503535);
            }
            $errorMessage = CitrixUtility::getErrorMessageForWebinarCancelRegistration($result['statusCode']);
            $this->addFlashMessage(
                $errorMessage,
                CitrixUtility::translate('registration.error.header'),
                AbstractMessage::ERROR
            );
            $this->redirect(
                'detail',
                null,
                null,
                [
                    'webinarKey' => $webinarKey
                ]
            );
        }
    }

    /**
     * Cancel webinar registration
     *
     * @param int $webinarKey
     * @return void
     */
    public function cancelRegistrationAction($webinarKey)
    {
        if (empty($webinarKey)) {
            throw new \Exception('Missing required parameter \'webinarKey\'', 43873404930);
        }

        $success = false;
        $result['statusCode'] = WebinarRequest::STATUS_CODE_CANCEL_REGISTRATION_NOT_REGISTERED;

        $registrant = $this->webinarRequest->getRegistrantByEmail($webinarKey, $this->frontendUser->getEmail());
        if (!empty($registrant)) {
            $registrantKey = $registrant['registrantKey'];
            if (empty($registrantKey)) {
                throw new \Exception('Missing registrantKey in registrant fetched from the API', 43873404990);
            }

            $result = $this->webinarRequest->removeRegistrant($webinarKey, $registrantKey);
            if (!empty($result)) {
                $success = $result['success'];
            }

            if ($success) {
                // Remove registration from database
                $this->webinarRegistrationRepository->removeWebinarRegistration(
                    $webinarKey,
                    $registrantKey,
                    $this->frontendUser->getEmail()
                );
            }
        }

        if ($success) {
            $this->addFlashMessage(
                CitrixUtility::translate('cancel_registration.success.message'),
                '',
                AbstractMessage::OK
            );
            $this->redirect(
                'detail',
                null,
                null,
                [
                    'webinarKey' => $webinarKey
                ]
            );
        } else {
            // If request failed then there must be statusCode so throw exception if
            // there is no statusCode
            if (empty($result['statusCode'])) {
                throw new \Exception('Missing statusCode', 3907503535);
            }

            $errorMessage = CitrixUtility::getErrorMessageForWebinarCancelRegistration($result['statusCode']);
            $this->addFlashMessage(
                $errorMessage,
                CitrixUtility::translate('cancel_registration.error.header'),
                AbstractMessage::ERROR
            );
            $this->redirect(
                'detail',
                null,
                null,
                [
                    'webinarKey' => $webinarKey
                ]
            );
        }
    }

    /**
     * prefillRegistrationFields
     *
     * @param array $registrationFields
     * @return array
     */
    protected function prefillRegistrationFields($registrationFields)
    {
        $feUserFieldToGetterKeys = array_keys($this->feUserFieldToGetter);
        foreach ($registrationFields['fields'] as &$field) {
            if (in_array($field['field'], $feUserFieldToGetterKeys)) {
                $field['value'] = $this->frontendUser->{$this->feUserFieldToGetter[$field['field']]}();
            }
        }
        return $registrationFields;
    }

    /**
     * prefillRegistrationFields
     *
     * @param array $registrationFields
     * @return array
     */
    protected function lockRegistrationFields($registrationFields)
    {
        if (!empty($this->settings['lockRegistrationFields'])) {
            foreach ($registrationFields['fields'] as &$field) {
                if ($this->isFieldLocked($field['field'])) {
                    $field['locked'] = true;
                }
            }
        }
        return $registrationFields;
    }

    /**
     * Check if field in webinar registration is locked
     *
     * @param string $field
     * @return bool
     */
    protected function isFieldLocked($field)
    {
        if (!$this->lockedFieldsArray) {
            $this->lockedFieldsArray = GeneralUtility::trimExplode(',', $this->settings['lockRegistrationFields']);
        }
        return in_array($field, $this->lockedFieldsArray);
    }
}
