<?php
namespace Pixelant\PxaCitrixIntegration\Utility;

/***
 *
 * This file is part of the "Citrix Integration for TYPO3" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Phat Hoang <phat@pixelant.se>, Pixelant
 *
 ***/

use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Pixelant\PxaCitrixIntegration\Citrix\Request\WebinarRequest;

/**
 * CitrixUtility
 */
class CitrixUtility
{
    /**
     * Localize
     *
     * @param string $key
     * @param array $arguments
     * @return string
     */
    public static function translate($key, $arguments)
    {
        return LocalizationUtility::translate($key, 'pxa_citrix_integration', $arguments);
    }

    /**
     * Get error message for webinar registration
     *
     * @param int $statusCode
     * @return string
     */
    public static function getErrorMessageForWebinarRegistration($statusCode)
    {
        $key = 'statusCode.registration.' . $statusCode;
        $translation = self::translate($key);

        return $translation ?? self::translate('statusCode.registration.default', [$statusCode]);
    }

    /**
     * Get error message for cancelling webinar registration
     *
     * @param int $statusCode
     * @return string
     */
    public static function getErrorMessageForWebinarCancelRegistration($statusCode)
    {
        $key = 'statusCode.cancel_registration.' . $statusCode;
        $translation = self::translate($key);

        return $translation ?? self::translate('statusCode.cancel_registration.default', [$statusCode]);
    }

    public static function getWebinarRequest()
    {
        $settings = $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_pxacitrixintegration_webinar.']['settings.'];
        return GeneralUtility::makeInstance(ObjectManager::class)
            ->get(WebinarRequest::class, $settings['accessToken'], $settings['organizerKey']);
    }
}
