<?php
namespace Pixelant\PxaCitrixIntegration\Service;

/***
 *
 * This file is part of the "Citrix Integration for TYPO3" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Phat Hoang <phat@pixelant.se>, Pixelant
 *
 ***/

use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;

/**
 * ConfigurationService
 */
class ConfigurationService implements \TYPO3\CMS\Core\SingletonInterface
{

    /**
     * @var string
     */
    protected $extensionKey = 'pxa_citrix_integration';

    /**
     * @var array
     */
    protected $extensionConfiguration = [];

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->extensionConfiguration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$this->extensionKey]);
    }

    /**
     * @return string
     */
    public function getExtensionKey()
    {
        return $this->extensionKey;
    }

    /**
     * @return array
     */
    public function getExtensionConfiguration()
    {
        return $this->extensionConfiguration;
    }
}
