<?php
namespace Pixelant\PxaCitrixIntegration\Domain\Model;

/***
 *
 * This file is part of the "Citrix Integration for TYPO3" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Phat Hoang <phat@pixelant.se>, Pixelant
 *
 ***/

/**
 * WebinarRegistration
 */
class WebinarRegistration extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * Webinar Key
     *
     * @var int
     * @validate NotEmpty
     */
    protected $webinarKey = '';

    /**
     * email
     *
     * @var string
     * @validate NotEmpty
     */
    protected $email = '';

    /**
     * registrantKey
     *
     * @var int
     * @validate NotEmpty
     */
    protected $registrantKey = '';

    /**
     * Returns the webinarKey
     *
     * @return int $webinarKey
     */
    public function getWebinarKey()
    {
        return $this->webinarKey;
    }

    /**
     * Sets the webinarKey
     *
     * @param int $webinarKey
     * @return void
     */
    public function setWebinarKey($webinarKey)
    {
        $this->webinarKey = $webinarKey;
    }

    /**
     * Returns the email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     *
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }


    /**
     * Returns the registrantKey
     *
     * @return int $registrantKey
     */
    public function getRegistrantKey()
    {
        return $this->registrantKey;
    }

    /**
     * Sets the registrantKey
     *
     * @param int $registrantKey
     * @return void
     */
    public function setRegistrantKey($registrantKey)
    {
        $this->registrantKey = $registrantKey;
    }
}
