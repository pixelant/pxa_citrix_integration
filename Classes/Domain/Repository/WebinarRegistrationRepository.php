<?php
namespace Pixelant\PxaCitrixIntegration\Domain\Repository;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use Pixelant\PxaCitrixIntegration\Domain\Model\WebinarRegistration;
use Pixelant\PxaCitrixIntegration\Citrix\Request\WebinarRequest;
use Pixelant\PxaCitrixIntegration\Utility\CitrixUtility;

/***
 *
 * This file is part of the "Citrix Integration for TYPO3" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Phat Hoang <phat@pixelant.se>, Pixelant
 *
 ***/

/**
 * The repository for Webinars
 */
class WebinarRegistrationRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * CreateWebinarRegistration
     *
     * @param int $webinarKey
     * @param int $registrantKey
     * @param string $email
     * @throws \Exception
     * @return WebinarRegistration|null The WebinarRegistration if creation was successful or null if not
     */
    public function createWebinarRegistration($webinarKey, $registrantKey, $email)
    {
        if (empty($webinarKey)) {
            throw new \Exception('Missing required parameter: \'webinarKey\'', 5409343110);
        }
        if (empty($registrantKey)) {
            throw new \Exception('Missing required parameter: \'registrantKey\'', 5409343120);
        }
        if (empty($email)) {
            throw new \Exception('Missing required parameter: \'email\'', 5409343130);
        }

        $webinarRegistration = null;
        if ($webinarKey && $registrantKey && $email) {
            $webinarRegistration = GeneralUtility::makeInstance(ObjectManager::class)
                ->get(WebinarRegistration::class);
            $webinarRegistration->setWebinarKey($webinarKey);
            $webinarRegistration->setRegistrantKey($registrantKey);
            $webinarRegistration->setEmail($email);
            $this->add($webinarRegistration);
        }
        return $webinarRegistration;
    }

    /**
     * RemoveWebinarRegistration
     *
     * @param int $webinarKey
     * @param int $registrantKey
     * @param string $email
     * @throws \Exception
     * @return bool True if the webinarRegistration was found and remove, otherwise false
     */
    public function removeWebinarRegistration($webinarKey, $registrantKey, $email)
    {
        if (empty($webinarKey)) {
            throw new \Exception('Missing required parameter: \'webinarKey\'', 5409343140);
        }
        if (empty($registrantKey)) {
            throw new \Exception('Missing required parameter: \'registrantKey\'', 5409343150);
        }
        if (empty($email)) {
            throw new \Exception('Missing required parameter: \'email\'', 5409343160);
        }

        $query = $this->createQuery();
        $query->matching(
            $query->logicalAnd(
                $query->equals('webinarKey', $webinarKey),
                $query->equals('registrantKey', $registrantKey),
                $query->equals('email', $email)
            )
        );
        $webinarRegistrations = $query->execute();
        if ($webinarRegistrations->count()) {
            $this->removeAll($webinarRegistrations);
            return true;
        }

        return false;
    }

    /**
     * Find all webinarRegistration objects and registrant conntected to the fe user's email
     * and filter out the ones that are no longer valid since cancelling can be done in for example
     * the confirmation e-mail
     *
     * @param string $email
     * @throws \Exception
     * @return array
     */
    protected function findRegisteredWebinarsAndRegistrantByEmail($email)
    {
        if (empty($email)) {
            throw new \Exception('Missing required parameter: \'email\'', 5509343160);
        }

        $webinarsAndRegistrationInfo = [];
        $webinarRegistrations = $this->findByEmail($email);
        $currentTimeUnix = time();
        foreach ($webinarRegistrations as $webinarRegistration) {
            $registrant = CitrixUtility::getWebinarRequest()->getRegistrant(
                $webinarRegistration->getWebinarKey(),
                $webinarRegistration->getRegistrantKey()
            );
            $webinar = CitrixUtility::getWebinarRequest()->getWebinar($webinarRegistration->getWebinarKey());

            // Check if both registration and registrationInfo could be fetched from API
            if (empty($registrant) || empty($webinar)) {
                // Don't do anything
            } else {
                $webinars[] = [
                    'webinar' => $webinar,
                    'registrant' => $registrant
                ];
            }
        }

        return $webinars;
    }

    /**
     * Find webinars to attend for user with the specific email
     *
     * @param string $email
     * @throws \Exception
     * @return array
     */
    public function findWebinardsToAttend($email)
    {
        if (empty($email)) {
            throw new \Exception('Missing required parameter: \'email\'', 5509343170);
        }

        $webinarsToAttend = [];
        $webinarsAndRegistrant = $this->findRegisteredWebinarsAndRegistrantByEmail($email);
        $currentTimeUnix = time();
        foreach ($webinarsAndRegistrant as $webinarAndRegistrant) {
            $webinar = $webinarAndRegistrant['webinar'];
            $endTimeUnix = strtotime(current($webinar['times'])['endTime']);
            if ($currentTimeUnix <= $endTimeUnix) {
                $webinarsToAttend[] = $webinar;
            }
        }
        return $webinarsToAttend;
    }

    /**
     * Find webinars attended webinars
     *
     * @param string $email
     * @param int $minimumAttendenceTimeInSeconds
     * @throws \Exception
     * @return array
     */
    public function findAttendedWebinars($email, $minimumAttendenceTimeInSeconds = 300)
    {
        if (empty($email)) {
            throw new \Exception('Missing required parameter: \'email\'', 5509343180);
        }

        $attendedWebinars = [];
        $webinarsAndRegistrant = $this->findRegisteredWebinarsAndRegistrantByEmail($email);
        $currentTimeUnix = time();
        foreach ($webinarsAndRegistrant as $webinarAndRegistrant) {
            $webinar = $webinarAndRegistrant['webinar'];

            // If endTime not passed, skip
            $endTimeUnix = strtotime(current($webinar['times'])['endTime']);
            if ($currentTimeUnix <= $endTimeUnix) {
                continue;
            }
            $registrantKey = $webinarAndRegistrant['registrant']['registrantKey'];
            $attendenceTimesTotal = 0;
            $attendeeSessions = CitrixUtility::getWebinarRequest()->getAttendeeSessions(
                $webinar['webinarKey'],
                $registrantKey
            );

            if (!empty($attendeeSessions)) {
                foreach ($attendeeSessions as $attendeeSession) {
                    $attendenceTimesTotal += $attendeeSession['attendanceTimeInSeconds'];
                }
            }

            if ($attendenceTimesTotal >= $minimumAttendenceTimeInSeconds) {
                $webinar['attendeeSessions'] = $attendeeSessions;
                $webinar['attendenceTimesTotal'] = $attendenceTimesTotal;
                $attendedWebinars[] = $webinar;
            }
        }

        return $attendedWebinars;
    }
}
